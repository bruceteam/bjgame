// All Rights reserved. BlackJack game by Oleg Sutugin

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BlackJack/Cards/Deck.h"
#include "BJGameMode.generated.h"

class ADeck;
class UNiagaraSystem;
class USoundCue;

USTRUCT(BlueprintType)
struct FGameplayData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	int PlayerScore = 0;
	UPROPERTY(BlueprintReadOnly)
	int DealerScore = 0;

	UPROPERTY(BlueprintReadOnly)
	bool bIsPlayerPassed = false;
	UPROPERTY(BlueprintReadOnly)
	bool bIsDealerPassed = false;

	UPROPERTY(BlueprintReadOnly)
	FString EndGameText;

	UPROPERTY(BlueprintReadOnly)
	bool bIsGameOver = false;

	UPROPERTY(BlueprintReadOnly)
	int PlayerCardSpawnDisplacement = -1700;
	UPROPERTY(BlueprintReadOnly)
	int DealerCardSpawnDisplacement = -1700;
};

UCLASS()
class BLACKJACK_API ABJGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	FGameplayData GetGameData() const { return GameplayData; }

protected:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ADeck> Deck;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
    UNiagaraSystem* MuzzleFX = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
    USoundCue* TakeCardSound;

	ADeck* myDeck;
	FGameplayData GameplayData;

	UFUNCTION(BlueprintCallable)
	void TakeCard(bool bIsFromPlayer);

	UFUNCTION(BlueprintCallable)
	void SetPlayerIsPassed();

	virtual void StartPlay() override;
	int SpawnCard(bool bIsFromPlayer);
	void StartRound();
private:
	void ShuffleArray();
	void InitializeStartValues();
	void SpawnFX(bool bIsFromPlayer);
	void FlipDealerCardAtTheEnd();
	FString SetStringWinOrLoseCondition(bool bIsFromPlayer);
	void EndGame();
	void PrintDeck();
};
