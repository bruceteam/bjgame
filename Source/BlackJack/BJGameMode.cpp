// All Rights reserved. BlackJack game by Oleg Sutugin


#include "BJGameMode.h"
#include "BlackJack/Cards/Card.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

//Function for debug only
void ABJGameMode::PrintDeck()
{
	for (int32 i = 0; i < myDeck->Deck.Num(); i++)
	{
		UE_LOG(LogTemp, Warning, TEXT("Card is: %s"), *myDeck->Deck[i]->GetName());
	}
}

//changing string value for win or lose
FString ABJGameMode::SetStringWinOrLoseCondition(bool bIsFromPlayer)
{
	if (bIsFromPlayer)
	{
		if (GameplayData.PlayerScore == 21)
		{
			return "YOU WIN";
		}
		else if (GameplayData.PlayerScore > 21)
		{
			return "YOU LOSE";
		}
		else
		{
			return "";
		}
	}
	else
	{
		if (GameplayData.DealerScore == 21)
		{
			return "YOU LOSE";
		}
		else if (GameplayData.DealerScore > 21)
		{
			return "YOU WIN";
		}
		else
		{
			return "";
		}
	}
}

//Draws first card for player and dealer
void ABJGameMode::StartRound()
{
	TakeCard(true);
	TakeCard(false);
}

//begin play options
void ABJGameMode::StartPlay()
{
	Super::StartPlay();

	auto PC = GetWorld()->GetFirstPlayerController();
	PC->SetInputMode(FInputModeUIOnly());
	PC->SetShowMouseCursor(true);

	myDeck = GetWorld()->SpawnActor<ADeck>(Deck);

	InitializeStartValues();

#if WITH_EDITOR
	PrintDeck();
#endif

	ShuffleArray();

#if WITH_EDITOR
	PrintDeck();
#endif

	StartRound();
}

/**	Takes card by Player(true) or Dealer(false)
	Changing next card spawn location displacement
	Checking win or lose condtion, for example - if PlayerScore = 21, then he immediately wins
	Dealer (if player passes) recursively taking cards, until he win or lose*/
void ABJGameMode::TakeCard(bool bIsFromPlayer)
{
	if (!GameplayData.bIsGameOver)
	{
		SpawnFX(bIsFromPlayer);

		if (bIsFromPlayer)
		{
			GameplayData.PlayerScore += SpawnCard(bIsFromPlayer);
			GameplayData.PlayerCardSpawnDisplacement += 370;
			GameplayData.EndGameText = SetStringWinOrLoseCondition(bIsFromPlayer);
		}
		else
		{
			GameplayData.DealerScore += SpawnCard(bIsFromPlayer);
			GameplayData.DealerCardSpawnDisplacement += 370;
			GameplayData.EndGameText = SetStringWinOrLoseCondition(bIsFromPlayer);
			if (GameplayData.bIsPlayerPassed && GameplayData.EndGameText == "")
			{
				if (GameplayData.DealerScore >= 16)
				{
					GameplayData.bIsDealerPassed = true;
				}
				else
				{
					TakeCard(bIsFromPlayer);
				}
			}
		}
		EndGame();
	}
}

//spawn card with diffrent location and rotation, and adds it Value to player or dealer
int ABJGameMode::SpawnCard(bool bIsFromPlayer)
{
	if (!GetWorld())
	{
		return 0;
	}

	if (bIsFromPlayer)
	{
		int CardIndex = FMath::RandRange(0, myDeck->Deck.Num() - 1);

		FVector SpawnLocation = FVector(0, GameplayData.PlayerCardSpawnDisplacement, 30);
		FRotator Rotation = FRotator::ZeroRotator;
		FActorSpawnParameters SpawnParams;

		ACard* NewCard = GetWorld()->SpawnActor<ACard>(myDeck->Deck[CardIndex], SpawnLocation, Rotation, SpawnParams);

		myDeck->Deck.RemoveAt(CardIndex);
		return NewCard->GetCardValue();
	}
	else
	{
		int CardIndex = FMath::RandRange(0, myDeck->Deck.Num() - 1);

		FVector SpawnLocation = FVector(800, GameplayData.DealerCardSpawnDisplacement, 30);
		FRotator Rotation = FRotator::ZeroRotator;
		Rotation.Roll = 180;
		FActorSpawnParameters SpawnParams;

		ACard* NewCard = GetWorld()->SpawnActor<ACard>(myDeck->Deck[CardIndex], SpawnLocation, Rotation, SpawnParams);

		myDeck->Deck.RemoveAt(CardIndex);
		return NewCard->GetCardValue();
	}
}

//Shuffling an array with math
void ABJGameMode::ShuffleArray()
{
	if (myDeck->Deck.Num() != 52)
	{
		return;
	}

	for (int32 i = myDeck->Deck.Num() - 1; i > 0; i--)
	{
		int32 j = FMath::Floor(FMath::Rand() * (i + 1)) % myDeck->Deck.Num();
		//fixing bug
		if (j < 0)
		{
			j = 0 - j;
		}
		UE_LOG(LogTemp, Warning, TEXT("j = %i"), j);
		myDeck->Deck.Swap(i, j);
	}
}

//Initialize values, when game starts
void ABJGameMode::InitializeStartValues()
{
	GameplayData.PlayerScore = 0;
	GameplayData.DealerScore = 0;
	GameplayData.bIsPlayerPassed = false;
	GameplayData.bIsDealerPassed = false;
	GameplayData.bIsGameOver = false;
}

//Spawn Visual and SoundEffects
void ABJGameMode::SpawnFX(bool bIsFromPlayer)
{
	FVector Location = bIsFromPlayer ? FVector(0, GameplayData.PlayerCardSpawnDisplacement, 750) : FVector(800, GameplayData.DealerCardSpawnDisplacement, 750);

	if (!GetWorld())
	{
		return;
	}

	if (MuzzleFX)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), MuzzleFX, Location);
	}

	if (TakeCardSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), TakeCardSound, Location);
	}
}


//if player passes - immediately dealer should pick a card
void ABJGameMode::SetPlayerIsPassed()
{
	GameplayData.bIsPlayerPassed = true;
	TakeCard(false);
}

//changing endgame values
void ABJGameMode::EndGame()
{
	if (GameplayData.EndGameText == "YOU WIN" || GameplayData.EndGameText == "YOU LOSE")
	{
		FlipDealerCardAtTheEnd();
		GameplayData.bIsGameOver = true;
		myDeck = nullptr;
	}
	else if (GameplayData.bIsPlayerPassed && GameplayData.bIsDealerPassed)
	{
		if (GameplayData.PlayerScore > GameplayData.DealerScore)
		{
			GameplayData.EndGameText = "YOU WIN";
		}
		else if (GameplayData.PlayerScore < GameplayData.DealerScore)
		{
			GameplayData.EndGameText = "YOU LOSE";
		}
		else
		{
			GameplayData.EndGameText = "DRAW";
		}

		FlipDealerCardAtTheEnd();
		GameplayData.bIsGameOver = true;
		myDeck = nullptr;
	}
}

void ABJGameMode::FlipDealerCardAtTheEnd()
{
	TArray<AActor*> CardsInGame;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ACard::StaticClass(), CardsInGame);

	for (auto Card : CardsInGame)
	{
		if (Card->GetActorLocation().X == 800)
		{
			//UE_LOG(LogTemp, Display, TEXT("Cards in Game = %i"), CardsInGame.Num());
			Card->SetActorRotation(FQuat(0, 0, 180, 0));
		}
	}
}