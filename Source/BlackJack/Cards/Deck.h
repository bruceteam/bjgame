// All Rights reserved. BlackJack game by Oleg Sutugin

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlackJack/Cards/Card.h"
#include "Deck.generated.h"

UCLASS()
class BLACKJACK_API ADeck : public AActor
{
	GENERATED_BODY()
	
public:	
	ADeck();

	UPROPERTY(EditDefaultsOnly)
	TArray<TSubclassOf<ACard>> Deck;
protected:
	virtual void BeginPlay() override;


private:
	//debug only
	bool CheckIsDeckFilled() const;
	bool CheckIsSuitsFilled() const;
	bool CheckIsRanksFilled() const;
};
