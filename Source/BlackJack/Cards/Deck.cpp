// All Rights reserved. BlackJack game by Oleg Sutugin


#include "BlackJack/Cards/Deck.h"
#include "BlackJack/Cards/Card.h"

ADeck::ADeck()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ADeck::BeginPlay()
{
	Super::BeginPlay();

	//Calls only in Editor, small sanity check
#if WITH_EDITOR
	CheckIsDeckFilled();
#endif
}


bool ADeck::CheckIsDeckFilled() const
{
	if (Deck.Num() != 52)
	{
		GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Red, "U r messed up with the number of cards!", true, FVector2D(2.0f, 2.0f));
		return false;
	}
	else if (!CheckIsSuitsFilled())
	{
		return false;
	}
	else if (!CheckIsRanksFilled())
	{
		return false;
	}

	GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Green, "Everything is fine with the deck", true, FVector2D(2.0f, 2.0f));
	return true;
}

/*
	Checking do we have 13 of each suit
*/
bool ADeck::CheckIsSuitsFilled() const
{
	int ClubsCounter = 0;
	int HeartsCounter = 0;
	int DiamondsCounter = 0;
	int SpadesCounter = 0;

	for (const auto& Card : Deck)
	{
		ACard* TempCard = Card.GetDefaultObject();
		ESuit CardSuit = TempCard->GetSuit();

		if (CardSuit == ESuit::Clubs)
		{
			ClubsCounter++;
		}
		else if (CardSuit == ESuit::Hearts)
		{
			HeartsCounter++;
		}
		else if (CardSuit == ESuit::Diamonds)
		{
			DiamondsCounter++;
		}
		else if (CardSuit == ESuit::Spades)
		{
			SpadesCounter++;
		}
	}

	if (ClubsCounter != 13 || HeartsCounter != 13 || DiamondsCounter != 13 || SpadesCounter != 13)
	{
		GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Red, "U r messed up with the Suits!", true, FVector2D(2.0f, 2.0f));
		return false;
	}
	else
	{
		return true;
	}
}

/*
	Checking do we have 4 of each rank
*/
bool ADeck::CheckIsRanksFilled() const
{
	int TwoCounter = 0;
	int ThreeCounter = 0;
	int FourCounter = 0;
	int FiveCounter = 0;
	int SixCounter = 0;
	int SevenCounter = 0;
	int EightCounter = 0;
	int NineCounter = 0;
	int TenCounter = 0;
	int JackCounter = 0;
	int QueenCounter = 0;
	int KingCounter = 0;
	int AceCounter = 0;

	for (const auto& Card : Deck)
	{
		ACard* TempCard = Card.GetDefaultObject();
		ERank CardRank = TempCard->GetRank();

		switch (CardRank)
		{
		case ERank::Two:
		{
			TwoCounter++;
			break;
		}
		case ERank::Three:
		{
			ThreeCounter++;
			break;
		}
		case ERank::Four:
		{
			FourCounter++;
			break;
		}
		case ERank::Five:
		{
			FiveCounter++;
			break;
		}
		case ERank::Six:
		{
			SixCounter++;
			break;
		}
		case ERank::Seven:
		{
			SevenCounter++;
			break;
		}
		case ERank::Eight:
		{
			EightCounter++;
			break;
		}
		case ERank::Nine:
		{
			NineCounter++;
			break;
		}
		case ERank::Ten:
		{
			TenCounter++;
			break;
		}
		case ERank::Jack:
		{
			JackCounter++;
			break;
		}
		case ERank::Queen:
		{
			QueenCounter++;
			break;
		}
		case ERank::King:
		{
			KingCounter++;
			break;
		}
		case ERank::Ace:
		{
			AceCounter++;
			break;
		}
		}
	}

	if (AceCounter != 4 || KingCounter != 4 || QueenCounter != 4 || JackCounter != 4 || //
		TenCounter != 4 || NineCounter != 4 || EightCounter != 4 || SevenCounter != 4 || //
		SixCounter != 4 || FiveCounter != 4 || FourCounter != 4 || ThreeCounter != 4 || TwoCounter != 4)
	{
		GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Red, "U r messed up with the Ranks!", true, FVector2D(2.0f, 2.0f));
		return false;
	}
	else
	{
		return true;
	}
}
