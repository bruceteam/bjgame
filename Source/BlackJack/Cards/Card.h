// All Rights reserved. BlackJack game by Oleg Sutugin

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Card.generated.h"

//Cards Suits
UENUM()
enum class ESuit : uint8
{
	Hearts UMETA(DisplayName = "Hearts"),
	Diamonds UMETA(DisplayName = "Diamonds"),
	Clubs UMETA(DisplayName = "Clubs"),
	Spades UMETA(DisplayName = "Spades")
};

//Cards Ranks
UENUM()
enum class ERank : uint8
{
	Two UMETA(DisplayName = "Two"),
	Three UMETA(DisplayName = "Three"),
	Four UMETA(DisplayName = "Four"),
	Five UMETA(DisplayName = "Five"),
	Six UMETA(DisplayName = "Six"),
	Seven UMETA(DisplayName = "Seven"),
	Eight UMETA(DisplayName = "Eight"),
	Nine UMETA(DisplayName = "Nine"),
	Ten UMETA(DisplayName = "Ten"),
	Jack UMETA(DisplayName = "Jack"),
	Queen UMETA(DisplayName = "Queen"),
	King UMETA(DisplayName = "King"),
	Ace UMETA(DisplayName = "Ace")
};

UCLASS()
class BLACKJACK_API ACard : public AActor
{
	GENERATED_BODY()
	
public:	
	ACard();

	ESuit GetSuit() const { return Suit; }
	ERank GetRank() const { return Rank; }
	int GetCardValue() const { return CardValue; }

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* CardMesh = nullptr;

	UPROPERTY(EditDefaultsOnly)
	ESuit Suit;

	UPROPERTY(EditDefaultsOnly)
	ERank Rank;

	void SetCardValue();
private:
	int CardValue;
};
