// All Rights reserved. BlackJack game by Oleg Sutugin


#include "BlackJack/Cards/Card.h"

// Sets default values
ACard::ACard()
{
	PrimaryActorTick.bCanEverTick = false;

	CardMesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	SetRootComponent(CardMesh);
}

void ACard::BeginPlay()
{
	Super::BeginPlay();

	check(CardMesh);

	check(Suit == ESuit::Hearts || Suit == ESuit::Diamonds || //
		  Suit == ESuit::Clubs || Suit == ESuit::Spades);

	check(Rank == ERank::Two || Rank == ERank::Three || //
		  Rank == ERank::Four || Rank == ERank::Five || //
		  Rank == ERank::Six || Rank == ERank::Seven || //
		  Rank == ERank::Eight || Rank == ERank::Nine || //
		  Rank == ERank::Ten || Rank == ERank::Jack || //
		  Rank == ERank::Queen || Rank == ERank::King || //
		  Rank == ERank::Ace);

	SetCardValue();
}

void ACard::SetCardValue()
{
	switch (Rank)
		{
		case ERank::Two:
		{
			CardValue=2;
			break;
		}
		case ERank::Three:
		{
			CardValue=3;
			break;
		}
		case ERank::Four:
		{
			CardValue=4;
			break;
		}
		case ERank::Five:
		{
			CardValue=5;
			break;
		}
		case ERank::Six:
		{
			CardValue=6;
			break;
		}
		case ERank::Seven:
		{
			CardValue=7;
			break;
		}
		case ERank::Eight:
		{
			CardValue=8;
			break;
		}
		case ERank::Nine:
		{
			CardValue=9;
			break;
		}
		case ERank::Ten:
		{
			CardValue=10;
			break;
		}
		case ERank::Jack:
		{
			CardValue=10;
			break;
		}
		case ERank::Queen:
		{
			CardValue=10;
			break;
		}
		case ERank::King:
		{
			CardValue=10;
			break;
		}
		case ERank::Ace:
		{
			CardValue=11;
			break;
		}
		}
}

